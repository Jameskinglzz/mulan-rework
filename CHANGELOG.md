## 0.0.16.2（2021-03-01）

报错信息改进：尝试解析错误所在行

## 0.0.16.1（2021-02-16）

测试，文档更新

## 0.0.16（2021-01-17）

[添加 try...catch；内置 min](https://zhuanlan.zhihu.com/p/345139002)

## 0.0.15.1（2020-12-10）

语法: 抛出（throw）; 内置函数: isa; 报错：属性错误信息修正

## 0.0.15.0 （2020-11-18）

[基于网络的运行环境；词法错误处理](https://zhuanlan.zhihu.com/p/301086221)

## 0.0.14.8 (2020-11-07)

[websocket 聊天演示；部分比较 Python 语法](https://zhuanlan.zhihu.com/p/277557485)

## 0.0.14.7 (2020-11-01)

[功能覆盖初版用户手册；Gitee Go 流水线尝鲜](https://zhuanlan.zhihu.com/p/271636727)

## 0.0.14.6 （2020-10-25）

[网络服务演示；with...as 的替代语法](https://zhuanlan.zhihu.com/p/268660973)

## 0.0.14.5 （2020-10-22）
修复： windows 下[引用模块问题](https://gitee.com/MulanRevive/mulan-rework/issues/I1U2HP)，现可运行井字棋与编辑器。

## 0.0.14.4 （2020-10-21）
[中文报错信息](https://zhuanlan.zhihu.com/p/267686876)

## 0.0.14.3 （2020-10-17）
[井字棋演示，tuple、枚举等](https://zhuanlan.zhihu.com/p/266428706)

## 0.0.14.1 （2020-10-14）
首次通过 ulang 包发布在 PyPI，[这里详细介绍](https://zhuanlan.zhihu.com/p/265695809)版本号解释、与之前逆向工程的对比等等